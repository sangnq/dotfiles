" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

"""Auto complete
Plug 'neoclide/coc.nvim', {'branch': 'release'}

"""NERDTree
"Plug 'scrooloose/nerdtree'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

""" Ultility
Plug 'machakann/vim-sandwich'
Plug 'jiangmiao/auto-pairs'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-commentary'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': ':UpdateRemotePlugins'}

""" FZF
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

""" Theme
Plug 'kaicataldo/material.vim'
Plug 'morhetz/gruvbox'
Plug 'hzchirs/vim-material'

""" Highlighting
Plug 'sheerun/vim-polyglot'

""" Git
Plug 'tpope/vim-fugitive'

""" Javascript syntax
Plug 'Yggdroot/indentLine'
Plug 'ryanoasis/vim-devicons'

" Initialize plugin system
call plug#end()

" General settings
set encoding=UTF-8
filetype plugin indent on         "Enabling Plugin & Indent
syntax on                         "Turning Syntax on
set autoread wildmode=longest,list,full
set autowrite autowriteall
set shiftwidth=4 autoindent smartindent tabstop=4 softtabstop=4 expandtab
set nospell                       " disable spelling
set ruler
set updatetime=100
set clipboard+=unnamedplus	      "Copy paster outside
"set ignorecase
set hls is ic
set laststatus=2 cmdheight=1
set backspace=indent,eol,start confirm
set noshowmode
set hidden			              " hidden closed buffer
set foldlevel=5
set splitbelow splitright
set signcolumn=no
set scrolloff=5
" set nu
set relativenumber

" Key-bindings
let mapleader = " "
map <C-z> :IndentLinesToggle<cr>
map <leader>w :w\|:noh<cr>
map <leader>p :Files<cr>
map <leader>[ :GitFiles <cr>
map <leader>] :Ag<space>
map <leader>} yiw:Ag<space><C-R><S-+><cr>
map <leader>b :Buffers <cr>
map <leader>q :q<cr>
map <silent> <leader>t :tabnew<bar>terminal<cr>i
map gb :Gblame<cr>
inoremap tn <Esc>
cnoremap <Tab> <CR>/<up>
cnoremap <S-Tab> <CR>?<up>
"map <silent> <leader>e :EditVifm .<CR>
map <silent> <leader>e :CHADopen<CR>
tnoremap <leader>\ <C-\><C-n>
nnoremap <Up> :resize +2<CR>
nnoremap <Down> :resize -2<CR>
nnoremap <Left> :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>

nnoremap <leader>d "_d
xnoremap <leader>d "_d
xnoremap <leader>p "_dP

nnoremap <leader>h <C-W>h
nnoremap <leader>j <C-W>j
nnoremap <leader>k <C-W>k
nnoremap <leader>l <C-W>l

xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv

" Delete current buffer, includes terminal buffer
map <F6> :bd!<cr>

" Close all buffer then open the last one
map <F7> :%bd!\|e#<cr>

" Close all buffers and quit Vim
map <F12> :%bd\|:q<cr>

" Color setting
set background=dark termguicolors cursorline
let g:material_theme_style = 'default-community' 
set t_Co=256
colorscheme material
"colorscheme vim-material

"Make the background transparent in some terminal
"hi! Normal ctermbg=NONE guibg=NONE
"hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE

"hi! GruvboxRedSign guibg=NONE
"hi! GruvboxAquaSign guibg=NONE
"hi! GruvboxOrangeSign guibg=NONE
"hi! GruvboxYellowSign guibg=NONE
"hi! SignColumn guibg=NONE
"hi! CursorLineNr guifg=NONE
"hi! LineNr guibg=#3c3836

"Status-line
set statusline=
set statusline+=%{FugitiveStatusline()}\ \ "
set statusline+=\ %{Filename()}
set statusline+=\ %m
set statusline+=\ %l/%L\ "
set statusline+=%="Right side settings
set statusline+=\ %y
set statusline+=\ %r
set statusline+=\[%n]
set statusline+=\ %{StatusDiagnostic()}\ \ "

function! StatusDiagnostic() abort
	  let info = get(b:, 'coc_diagnostic_info', {})
	  let msgs = []
	  if get(info, 'error', 0)
	    call add(msgs, 'E:' . info['error'])
	  endif
	  if get(info, 'warning', 0)
	    call add(msgs, 'W:' . info['warning'])
	  endif
	  return join(msgs, ' ').' '
	endfunction

function! Filename()
  let root = fnamemodify(get(b:, 'git_dir'), ':h')
  let path = expand('%:p')
  if path[:len(root)-1] ==# root
    return path[len(root)+1:]
  endif
  return expand('%')
endfunction

"Minimal nerdtree
let NERDTreeMinimalUI=1
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let g:NERDTreeWinSize=40

"Auto open nerdtree on start
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Auto reload file changes: check one time after 4s of inactivity in normal mode
set autoread
au CursorHold * checktime

" FZF settings
" ignore dir in .ignore file when using :Files
let $FZF_DEFAULT_COMMAND = 'ag -g ""'
let g:fzf_buffers_jump = 1
let g:fzf_preview_window = ''
let $FZF_DEFAULT_OPTS=' --layout=reverse  --margin=1,2'
let g:fzf_layout = { 'window': 'call FloatingFZF()' }

function! FloatingFZF()
  let buf = nvim_create_buf(v:false, v:true)
  call setbufvar(buf, '&signcolumn', 'no')

  let height = float2nr(20)
  let width = float2nr(200)
  let horizontal = float2nr((&columns - width) / 2)
  let vertical = 1

  let opts = {
        \ 'relative': 'editor',
        \ 'row': vertical,
        \ 'col': horizontal,
        \ 'width': width,
        \ 'height': height,
        \ 'style': 'minimal'
        \ }

  call nvim_open_win(buf, v:true, opts)
endfunction

" indent line
let g:indentLine_char = '┆'          "['|', '¦', '┆', '┊']
let g:indentLine_enabled = 0

" vim closetag
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.jsx,*.js'
let g:closetag_xhtml_filenames = '*.xhtml,*.jsx,*.js'
let g:closetag_xhtml_filetypes = 'xhtml,jsx,js'
let g:closetag_emptyTags_caseSensitive = 1
let g:closetag_shortcut = '>'
let g:closetag_close_shortcut = '<leader>>'

" Autosave buffers before leaving them
autocmd BufLeave * silent! :wa

" Remove trailing white spaces on save
autocmd BufWritePre * :%s/\s\+$//e

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
" inoremap <silent><expr> <leader><ENTER> coc#refresh()

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
"autocmd CursorHold * silent call CocActionAsync('highlight')
augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end
